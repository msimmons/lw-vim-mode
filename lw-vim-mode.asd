(defsystem "lw-vim-mode"
    :author "Larry Clapp <larry@theclapp.org>"
    :license "LLGPL"
    :description "A Vim mode for the LispWorks Editor."
    :serial t
    :components
    ((:file "packages")
     (:file "wrap-editor")
     (:file "vars")
     (:file "classes")
     (:file "vim-vars")
     (:file "def-stuff")
     (:file "macros")
     (:file "functions")
     (:file "commands")
     (:file "bindings")
     ))

